from django.core.mail import send_mail
from django.test import TestCase
from django.core import mail
from main import forms
from main.forms import logger


class TestForm(TestCase):
  def test_valid_contact_us_form_sends_email(self):
    form = forms.ContactForm(
      {"name": "Luke Skywalker", "message": "Hi there"}
    )

    self.assertTrue(form.is_valid())

    with self.assertLogs("main.forms", level="INFO") as cm:
      form.send_mail()

    self.assertEqual(len(mail.outbox), 1)
    self.assertEqual(mail.outbox[0].subject, "Site message")

    self.assertGreaterEqual(len(cm.output), 1)

  def test_invalid_contact_us_form(self):
    form = forms.ContactForm({"message": "Hi there"})

    self.assertFalse(form.is_valid())

  def send_mail(self):
    logger.info("Sending email to customer service")
    message = "From: {0}\n{1}".format(
      self.cleaned_data["name"],
      self.cleaned_data["message"],
    )

    send_mail(
      "Site message",
      message,
      "site@booktime.domain", ["customerservice@booktime.domain"],
      fail_silently=False,
    )
