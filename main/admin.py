from django.contrib import admin

from . import models
from django.utils.html import format_html


class ProductAdmin(admin.ModelAdmin):
  list_display = ('name', 'slug', 'in_stock', 'price')
  list_filter = ('active', 'in_stock', 'date_updated')
  list_editable = ('in_stock',)
  search_fields = ('name',)
  prepopulated_fields = {"slug": ("name",)}

def make_active(self, request, queryset):
  queryset.update(active=True)


make_active.short_description = "Mark selected items as active"


def make_inactive(self, request, queryset):
  queryset.update(active=False)


make_inactive.short_description = (
  "Mark selected items as inactive"
)


class ProductAdmin(admin.ModelAdmin):
  list_display = ("name", "slug", "in_stock", "price")
  list_filter = ("active", "in_stock", "date_updated")
  list_editable = ("in_stock",)
  search_fields = ("name",)
  prepopulated_fields = {"slug": ("name",)}
  autocomplete_fields = ("tags",)
  actions = [make_active, make_inactive]

  def get_readonly_fields(self, request, obj=None):
    if request.user.is_superuser:
      return self.readonly_fields
    return list(self.readonly_fields) + ["slug", "name"]

  def get_prepopulated_fields(self, request, obj=None):
    if request.user.is_superuser:
      return self.prepopulated_fields
    else:
      return {}


class ProductTagAdmin(admin.ModelAdmin):
  list_display = ("name", "slug")
  list_filter = ("active",)
  search_fields = ("name",)
  prepopulated_fields = {"slug": ("name",)}

  def get_readonly_fields(self, request, obj=None):
    if request.user.is_superuser:
      return self.readonly_fields
    return list(self.readonly_fields) + ["slug", "name"]

  def get_prepopulated_fields(self, request, obj=None):
    if request.user.is_superuser:
      return self.prepopulated_fields
    else:
      return {}


class ProductImageAdmin(admin.ModelAdmin):
  list_display = ("thumbnail_tag", "product_name")
  readonly_fields = ("thumbnail",)
  search_fields = ("product__name",)

  def thumbnail_tag(self, obj):
    if obj.thumbnail:
      return format_html(
        '<img src="%s"/>' % obj.thumbnail.url
      )
    return "-"

  thumbnail_tag.short_description = "Thumbnail"

  def product_name(self, obj):
    return obj.product.name


# admin.site.register(models.Product)
admin.site.register(models.Product, ProductAdmin)
admin.site.register(models.ProductTag, ProductTagAdmin)
admin.site.register(models.ProductImage, ProductImageAdmin)
